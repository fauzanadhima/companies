@extends('layouts.app')

@section('content')

    <section id="description" class="card">
        <div class="card-header">
            <h4 class="card-title">Edit Employees</h4>
        </div>
        <div class="card-content">
            <div class="card-body">

                {!! Form::model($data,['route' => ['employees.update', $data->id],'files' => true, 'method' => 'put', 'id' => 'form-task'])!!}
                <div class="form-body">
                    <div class="row">
                        <div class="col md-6">
                            <div class="form-group">
                                {!! Form::label('nama', 'Nama') !!}
                                {!! Form::text('nama', null, ['id' => 'nama', 'class' => 'form-control', 'placeholder' => 'Nama']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('email', 'Email') !!}
                                {!! Form::email('email', null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Email']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col md-6">
                            <div class="form-group">
                                {!! Form::label('company_id', 'Companies') !!}
                                {!! Form::select('company_id', $com ,null, ['id' => 'company_id', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <a href="{{route('employees.index')}}" class="btn btn-warning mr-1" onclick="return confirm('Apa anda yakin?')">
                        <i class="feather icon-corner-down-left"></i> Back
                    </a>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check-square-o"></i> Submit
                    </button>
                </div>
                {!! Form::close()  !!}
            </div>
        </div>
    </section>
@endsection

@push('js')
    {!! JsValidator::formRequest('App\Http\Requests\Employees\CreateRequest') !!}
@endpush
