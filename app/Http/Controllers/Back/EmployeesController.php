<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Companies;
use App\Models\Employees;
use Illuminate\Http\Request;
use Session;
use DataTables;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = Employees::with(['company'])->select('*');

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($a) {
                    return '<a href="' . route('employees.edit', $a->id) . '" class="primary edit mr-1"><i class="fa fa-pencil mr-2"></i></a>
                            <a href="' . route('employees.destroy', $a->id) . '" class="danger delete delete-data-table mr-1" ><i class="fa fa-trash mr-2"></i></a>';
                })
                ->make(true);
        }

        return view('page.employees.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Companies::pluck('nama', 'id');
        return view('page.employees.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Employees::create(
            [
                'nama' => $request->nama,
                'email' => $request->email,
                'company_id' => $request->company_id,
            ]
        );

        if($data){
            Session::flash('keterangan', 'Data berhasil di simpan');
        }

        return redirect()->route('employees.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Employees::find($id);
        $com = Companies::pluck('nama', 'id');
        return view('page.employees.edit', compact('data', 'com'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Employees::find($id)
            ->update($request->except('_token', '_method', 'proengsoft_jsvalidation'));
        if($data){
            Session::flash('keterangan', 'Data berhasil di simpan');
        }

        return redirect()->route('employees.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Employees::destroy($id);

        return $data;
    }
}
