<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Buku;
use Illuminate\Http\Request;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Buku::select('*')->get();

        return response()->json(compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Buku::create(
            [
                'kode_buku' => $request->kode_buku,
                'judul_buku' => $request->judul_buku,
                'tahun_terbit' => $request->tahun_terbit,
                'stok_buku' => $request->stok_buku,
                'penulis' => $request->penulis,
            ]
        );

        return response()->json(compact('data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Buku::where('kode_buku', $id)->first();

        return response()->json(compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Buku::where('kode_buku', $id)->update(
            [
                'kode_buku' => $request->kode_buku,
                'judul_buku' => $request->judul_buku,
                'tahun_terbit' => $request->tahun_terbit,
                'stok_buku' => $request->stok_buku,
                'penulis' => $request->penulis,
            ]
        );

        return response()->json(compact('data'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Buku::where('kode_buku',$id)->delete();

        return response()->json(compact('data'));
    }
}
